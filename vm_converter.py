# 
# VMWare to Proxmox convertion tool
#
# Now that I have a great domain name, I can start
# to work on my novel.
#


from pyVim import connect
from pyVmomi import vim, vmodl
from proxmoxer import ProxmoxAPI

import pyVmomi
import operator
import time
import subprocess
import os
import re
import config
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-n', '--name', required=True, help='Source VM name')
args = vars(parser.parse_args())

vm_name = args['name']

proxmox_api_host = config.PROXMOX_HOST['host']
proxmox_user = config.PROXMOX_HOST['user']
proxmox_pw = config.PROXMOX_HOST['password']
proxmox_ssl = config.PROXMOX_HOST['verify_ssl']

vc_host = config.VC_HOST['host']
vc_user = config.VC_HOST['user']
vc_pw = config.VC_HOST['password']

vm_s_path = config.VM_CONVERT['vm_source_path']
proxmox_d_path = config.VM_CONVERT['proxmox_destination_path']
proxmox_storage = config.VM_CONVERT['proxmox_storage_name']
proxmox_host = config.VM_CONVERT['proxmox_destination_host']

def proxmox():
    cluster_proxmox = ProxmoxAPI(proxmox_api_host, user=proxmox_user, password=proxmox_pw, verify_ssl=proxmox_ssl)
    return cluster_proxmox

def vc():
    cluster_vc = connect.SmartConnectNoSSL(host=vc_host, port=443, user=vc_user, pwd=vc_pw)
    return cluster_vc 

def convert_vmdk_qcow(s_disk, d_path, d_disk):
    print("Converting " + s_disk + " disk to " + d_disk + "...")
      
    # tidy up paths and create destination directory
    if not d_path.endswith('/'):
        d_path = d_path + "/"
        
    if not os.path.exists(d_path):
        os.makedirs(d_path)

    if not os.path.isfile(s_disk):
        print(s_disk + " does not exist.. exiting!")
        exit()
        
    # use qemu-img to convert from VMDK to QCOW2 disks and put them into the correct location
    command = "/usr/bin/qemu-img convert -O qcow2 " + s_disk + " " + d_path + d_disk
    #print("Running " + command)
    print("This may take some time...")
    try:
        subprocess.call(command, shell=True)
    except subprocess.CalledProcessError as e:
        print("Error converting files: " + e.output)
        exit()
    
def GetProperties(content, viewType, props, specType):
    # Build a view and get basic properties for all Virtual Machines
    objView = content.viewManager.CreateContainerView(content.rootFolder, viewType, True)
    tSpec = vim.PropertyCollector.TraversalSpec(name='tSpecName', path='view', skip=False, type=vim.view.ContainerView)
    pSpec = vim.PropertyCollector.PropertySpec(all=False, pathSet=props, type=specType)
    oSpec = vim.PropertyCollector.ObjectSpec(obj=objView, selectSet=[tSpec], skip=False)
    pfSpec = vim.PropertyCollector.FilterSpec(objectSet=[oSpec], propSet=[pSpec], reportMissingObjectsInResults=False)
    retOptions = vim.PropertyCollector.RetrieveOptions()
    totalProps = []
    retProps = content.propertyCollector.RetrievePropertiesEx(specSet=[pfSpec], options=retOptions)
    totalProps += retProps.objects
    while retProps.token:
        retProps = content.propertyCollector.ContinueRetrievePropertiesEx(token=retProps.token)
        totalProps += retProps.objects
    objView.Destroy()
    # Turn the output in retProps into a usable dictionary of values
    gpOutput = []
    for eachProp in totalProps:
        propDic = {}
        for prop in eachProp.propSet:
            propDic[prop.name] = prop.val
        propDic['moref'] = eachProp.obj
        gpOutput.append(propDic)
    return gpOutput

def get_vlan(content, vimtype, key):
    vlan = None
    container = content.viewManager.CreateContainerView(content.rootFolder, vimtype, True)
    for c in container.view:
        if c.key == key:
            #print(c.config.defaultPortConfig.vlan.vlanId)
            vlan = c.config.defaultPortConfig.vlan.vlanId
            break
    return vlan

def proxmox_create_vm(specs, disks):
    print("Creating VM in Proxmox...")
    print(specs['name'])
        
    # Built the network and disk strings
    # By including the old VMWare MAC the VM should pick up the same IP address
    # from its config.
    if int(specs['nics']) > 0:
        net0 = "virtio={},bridge={},tag={}".format(specs['net1-mac'], config.VM_CONVERT['proxmox_trunk_network'], specs['net1-vlan'])
    else:
        net0 = "bridge={}".format(config.VM_CONVERT['proxmox_trunk_network'])
    
    cluster_proxmox = proxmox()
    node = cluster_proxmox.nodes(proxmox_host)
    # Create the basic VM in Proxmox
    # Disks added later
    node.qemu.create(
        vmid=specs['vmid'],
        memory=specs['memory'],
        sockets=1,
        scsihw='virtio-scsi-pci',
        cores=specs['cores'],
        name=specs['name'],
        net0=net0
    )
    
    time.sleep(3)
    # Add all disks in order of SCSI ID from VMware
    # Note: the disks must exist to use the 'config.set' to add them.
    # VMWare starts disks at 1, Promox starts disks at 0
    disk_num = 1
    for key, value in sorted(disks.items(), key=lambda item: item[0]):
        if "size" in key:
            scsi = "{}:{}/vm-{}-disk-{}.qcow2,size={}".format(proxmox_storage, specs['vmid'], specs['vmid'], disk_num, value)
            print("Adding " + scsi)
            pass_str = { "scsi{}".format(disk_num - 1): scsi }
            node.qemu(specs['vmid']).config.set(**pass_str)
            disk_num = disk_num + 1
    
    time.sleep(3)
    # Set boot disk to scsi0
    node.qemu(specs['vmid']).config.set(bootdisk='scsi0')
    
    # Add extra network cards if required.
    # NOTE: the script uses the VLAN tag from VMware but the networks must exist on Proxmox
    if int(specs['nics']) > 1:
        for i in range(1, int(specs['nics'])):
            print("Adding Nic {}".format(i))
            if i == 1:
                net1 = "virtio={},bridge={},tag={}".format(specs['net2-mac'], config.VM_CONVERT['proxmox_trunk_network'], specs['net2-vlan'])
                print("Adding NIC " + net1)
                node.qemu(specs['vmid']).config.set(net1=net1)
            if i == 2:
                net2 = "virtio={},bridge={},tag={}".format(specs['net3-mac'], config.VM_CONVERT['proxmox_trunk_network'], specs['net3-vlan'])
                print("Adding NIC " + net2)
                node.qemu(specs['vmid']).config.set(net2=net2)
            if i == 3:
                net3 = "virtio={},bridge={},tag={}".format(specs['net4-mac'], config.VM_CONVERT['proxmox_trunk_network'], specs['net4-vlan'])
                print("Adding NIC " + net3)
                node.qemu(specs['vmid']).config.set(net3=net3)            
            if i == 4:
                net4 = "virtio={},bridge={},tag={}".format(specs['net5-mac'], config.VM_CONVERT['proxmox_trunk_network'], specs['net5-vlan'])
                print("Adding NIC " + net4)
                node.qemu(specs['vmid']).config.set(net4=net4)                     


def vc_get_vm():
    print("Collection VM configuration for {}".format(vm_name))
    cluster_vc = vc()
    content = cluster_vc.RetrieveContent()
    retProps = GetProperties(content, [vim.VirtualMachine], ['name', 'runtime.powerState'], vim.VirtualMachine)
    
    my_vm = {}
    my_vm_scsi = {}
    my_vm_disk = {}    
    
    for vm in retProps:
        if vm['name'] == vm_name:
            if vm['runtime.powerState'] == "poweredOn":
                input('The VM is still powered on, shut down the VM and press enter to continue')
            #if vm.rootSnapshot:
                #input('The VM has a snapshot, remove the snapshot and press enter to continue')
                
            summary = vm['moref'].summary
            my_vm = {'name': vm['name']}
            vmid = str(summary.vm).replace("vim.VirtualMachine:vm-", "")
            vmid = vmid.replace("'", "")            
            my_vm.update({'vmid': str(vmid)})
            my_vm.update({'memory': str(summary.config.memorySizeMB)})
            my_vm.update({'cores': str(summary.config.numCpu)})
            my_vm.update({'quest_type': summary.config.guestId})
            my_vm.update({'disks': str(summary.config.numVirtualDisks)})
            my_vm.update({'nics': str(summary.config.numEthernetCards)})
            my_vm.update({'ip': str(summary.guest.ipAddress)})
    
            vm_hardware = vm['moref'].config.hardware
            
            # Find the scsi controllers and their device IDs
            for each_vm_hardware in vm_hardware.device:
                if (each_vm_hardware.key >= 1000) and (each_vm_hardware.key < 2000):
                    my_vm_scsi.update({'{}'.format(each_vm_hardware.key): each_vm_hardware.busNumber})        
                        
            # Find and add disks and nics
            for each_vm_hardware in vm_hardware.device:
                if (each_vm_hardware.key >= 2000) and (each_vm_hardware.key < 3000):
                    drive = each_vm_hardware.deviceInfo.label
                    
                    # Work out the disk order using the scsi controller ID plus scsi channel ID
                    disk_order = my_vm_scsi[str(each_vm_hardware.controllerKey)] * 10
                    disk_order = disk_order + each_vm_hardware.unitNumber
                    
                    # Key the disks dict on the disk_order so it can be sorted later
                    # we will convert the disks in the correct order for mounting
                    my_vm_disk.update({'{}-file'.format(disk_order): each_vm_hardware.backing.fileName})
                    my_vm_disk.update({'{}-size'.format(disk_order): '{:.1f}G'.format(each_vm_hardware.capacityInKB/1024/1024)})
                elif (each_vm_hardware.key >= 4000) and (each_vm_hardware.key < 5000):
                    nic = each_vm_hardware.deviceInfo.label
                    nic_i = nic.replace("Network adapter ", "")
                    # Collect the MAC address of each NIC so the MAC can be recreated on Proxmox
                    my_vm.update({'net{}-mac'.format(nic_i): each_vm_hardware.macAddress})
                    # Get the dvSwitch port group key for the NIC
                    dv_pg_key = each_vm_hardware.backing.port.portgroupKey
                    # Get the VLAN ID from the port group
                    vlan = get_vlan(content, [vim.dvs.DistributedVirtualPortgroup], dv_pg_key)
                    my_vm.update({'net{}-vlan'.format(nic_i): vlan})
                    
    connect.Disconnect(cluster_vc)
    return(my_vm, my_vm_disk)
                
def main():
    # Get all the specs from the VMware API about the virtual machine
    my_vm, my_vm_disk = vc_get_vm()

    # Find source and destination disk locations
    # VMWare lists the disk as the VMDK file but we need the 'flat' VMDK file
    disk_num = 1
    for key, value in sorted(my_vm_disk.items(), key=lambda item: item[0]):
        print("convert - {}: {}".format(key, value))
        # Get disk file details for conversion
        if "file" in key:
            s_disk = re.sub("\[.*?\] ", "", value)
            s_disk = s_disk.replace(".vmdk", "-flat.vmdk")
            s_disk = vm_s_path + s_disk
            d_path = proxmox_d_path + my_vm['vmid']
            d_disk = "vm-{}-disk-{}.qcow2".format(my_vm['vmid'], disk_num)
        
            # Convert the main disk
            convert_vmdk_qcow(s_disk, d_path, d_disk)
            disk_num = disk_num + 1

    proxmox_create_vm(my_vm, my_vm_disk)

if __name__ == "__main__":
    main()