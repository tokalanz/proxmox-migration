# Proxmox host for API calls
PROXMOX_HOST = {
    'host': 'IP',
    'user': 'USER@pam',
    'password': "PASSWORD",
    'verify_ssl': False
}

# vCenter detail for API calls
VC_HOST = {
    'host': 'VSPHERE',
    'user': 'USERNAME',
    'password': "PASSWORD"
}

# vm_source_path: physical path to the VMDK files
# proxmox_destination_host: the name of the destination Proxmox host
# proxmox_destination_path: physical path to where the Proxmox images (QCOW2 files) are stored. This must be valid storage for Proxmox
# proxmox_storage_name: Proxmox storage resource name (Shared storage ID)
# proxmox_trunk_network: network bridge for trunked vlans

VM_CONVERT = {
    'vm_source_path': '/mnt/pve/vmware/',
    'proxmox_destination_host': 'pve-01',
    'proxmox_destination_path': '/mnt/pve/images/',
    'proxmox_storage_name': 'storage1',
    'proxmox_trunk_network': 'vmbr0'
}

