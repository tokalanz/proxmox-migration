VMware to Proxmox migration script uses the VMware API to collect configuration details of the virtual machine and then use the Proxmox API to create a new instance of the virtual machine.
The migration script converts the VMKD disk files into QCOW2 disk files using the external command line utility qemu-img.
While collecting the virtual machine configuration from VMware, the script reorders the disks based on SCSI controller and channel order.
All VLAN tags are also collected along witg MAC address of the network cards. The MAC addresses and VLAN tags are replicated on the Proxmox environment.


## Installation:

This script is written to use Python 3.

Install pip3 to install the required Python packages as well as the qemu-utils package to allow VMDK to QCOW2 conversion.

> apt-get install python3-pip

> apt-get install qemu-utils

> pip3 install pyVmomi

> pip3 install proxmoxer


## Configuration:

The config.py file contains all parameters that need to be set to use the script.

PROXMOX_HOST: *This is the login details needed to access the Proxmox API which is used to create the VMs.*

VC_HOST: *This is the login details needed to access the vCentre API to collect the VM specifications.*


vm_source_path: *The physical path to the VMDK files. The path must be visable from the server the script is run from.*

proxmox_destination_host: *The name for the destination Proxmox host which the VMs will be created on. Once created the VM can be migrated to another host.*

proxmox_destination_path: *The physical path to the QCOW2 files used by Proxmox. The path must be visible/writable from the server the script is run from.*

proxmox_storage_name: *Proxmox storage resource name, this is the Proxmox shared storage ID.*

proxmox_trunk_network: *The network bridge adaptor used for trunked vlans e.g vmbr2*


## Usage:

The script used the VM name as the key to find the specifications and disks.

The script only requires one arguement, --name

> python3 vm_converter.py --name <vm name>

e.g

> python3 vm_converter.py --name my-vm-001

Note: You will need to manually shut down the source VM and manually start the destination VM.

